import os

count = False
print(
    "----------------------------------\n" + 
    "| Remove .DS_Store Files Utility |\n" +
    "----------------------------------")
directory = input("ABSOLUTE Path to remove .DS_Store: ")

if ("--count" in directory):
    directory = directory.replace("--count", "")
    count = True

directory = directory.strip()
if (not count):
    print("Removing files in \'{}\'...".format(directory))
else:
    print("Counting files in \'{}\'...".format(directory))

stores = 0
for subdir, dirs, files in os.walk(directory):
    for file in files:
        filepath = subdir + os.sep + file
        # print(filepath)
        if (file.endswith(".DS_Store")):
            stores += 1
            if (not count): os.remove(filepath)

if (not count):
    print("Removed {} \'.DS_Store\' files in \'{}\'.".format(stores, directory))
else:
    print("Found {} \'.DS_Store\' files in \'{}\'.".format(stores, directory))