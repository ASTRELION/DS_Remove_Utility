# DS Remove Utility
Short Python3 script to remove all .DS_Store files found in a directory. I use this because I sync all my files across multiple devices and do not want .DS_Store to sync.

### What's a .DS_Store File?
.DS_Store files are files used by macOS for displaying certain characteristics of folders and files.

## How To Use
1. Clone the repo, eg. `git clone https://github.com/ASTRELION/DS_Remove_Utility.git`
2. cd into DS_Remove_Utility, eg. `cd DS_Remove_Utility`
3. Run `python3 ds_remove_utility.py`
4. Follow command prompts that follow